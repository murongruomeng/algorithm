/*
 * fatal_error.h
 *
 *  Created on: 2014��9��2��
 *      Author: admin
 */

#include <stdlib.h>
#include <stdio.h>

#define error(str) fatalError(str)
#define fatalError(str) fprintf(stderr, "%s\n", str), exit(1)
