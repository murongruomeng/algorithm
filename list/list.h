/*
 * list.h
 *
 *  Created on: 2014年9月2日
 *      Author: admin
 */

#ifndef LIST_H_
#define LIST_H_

#define LIST_START_HEAD 0
#define LIST_START_TAIL 1

typedef int elementType;

typedef struct listNode{
	struct listNode *prev;
	struct listNode *next;
	elementType value;
} listNode;


typedef struct  listIter{
	listNode *next;
	int direction;
} listIter;

typedef struct list{
	listNode *head;
	listNode *tail;
	unsigned long len;
} list;


list *listInit(void);
void listRelease(list *list);
list *listAddNodeHead(list *list, elementType value);
list *listAddNodeTail(list *list, elementType value);

//插入在指定节点之后
list *listInsertNode(list *list, listNode *before_node, elementType value);
void listDelNode(list *list, listNode *n);
listIter *listGetIterator(list *list, int direction);
listNode *listNext(listIter *iter);

//返回第一个找到的node
listNode *listFind(list *list , elementType value);
listNode *listFindLast(list *list, elementType value);

#endif /* LIST_H_ */
