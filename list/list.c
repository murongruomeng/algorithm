/*
 * list.c
 *
 *  Created on: 2014��9��2��
 *      Author: admin
 */

#include <stdlib.h>
#include "../list/list.h"
#include "../utils/fatal_error.h"

list* listInit(void){

	struct list *list;

	list = malloc(sizeof(*list));

	if(list == NULL)
		fatalError("Out of memory!!!");

	list->head = list->tail = NULL;
	list->len = 0;

	return list;
}

list* listAddNodeTail(list *list, elementType value){


		listNode *node;
		node = malloc(sizeof(*node));

		if(node == NULL)
			fatalError("Out of memory!!!");

		node->next = NULL;
		node->value = value;

		if(list->len == 0){
			node->prev = NULL;
			list->head = list->tail = node;
		}
		else{
			node->prev = list->tail;
			list->tail->next = node;
			list->tail = node;
		}
		list->len ++;

		return list;
}

list *listAddNodeHead(list *list, elementType value){


			listNode *node ;
			node = malloc(sizeof(*node));
			if(node == NULL)
				fatalError("Out of memory!!!");

			node->value = value;
			node->prev = NULL;
			if(list->len == 0){
				node->next = NULL;
				list->head = list->tail = node;
			}
			else{
				node->next = list->head;
				list->head->prev = node;
				list->head = node;
			}
			list->len ++;

			return list;

	return list;
}

listNode *listFind(list *list , elementType value){

	listIter *iter;
	listNode *node;

	iter = listGetIterator(list, LIST_START_HEAD);
	while((node = listNext(iter)) != NULL){
		if(node->value == value)
			return node;
	}

	return NULL;
}

//list *listInsertNode(list *list, node *before_node, elementType value){
//
//
//}

listIter *listGetIterator(list *list, int direction){
	listIter *iter ;
	iter = malloc(sizeof(*iter));

	if(iter == NULL)
		fatalError("Out of memory!!!");

	if(direction == LIST_START_HEAD)
		iter->next = list->head;
	else
		iter->next = list->tail;

	iter->direction = direction;
	return iter;
}

listNode *listNext(listIter *iter){
	listNode *current = iter->next;

	if(current != NULL){
		if(iter->direction == LIST_START_HEAD)
			iter->next = current->next;
		else
			iter->next = current->prev;
	}
	return current;
}

